Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |  [de](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/01_operating_manual/S3-0014_B2_BA_Die%20Eject%20Unit.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/01_operating_manual/S3-0014_A1_OM_Die%20Eject%20Unit.pdf)                  |
| assembly drawing         | [de](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/02_assembly_drawing/s3-0014_D_ZNB_die_eject_unit.pdf)                 |
| circuit diagram          |   [de](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/03_circuit_diagram/S3-0014-EPLAN-B.pdf)               |
| maintenance instructions |     [de](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/04_maintenance_instructions/S3-0014_A_Wartungsanweisungen.pdf)             |
| spare parts              | [de](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/05_spare_parts/S3-0014_B1_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0014-die-eject-unit-deu-8/-/raw/main/05_spare_parts/S3-0014_A1_SWP.pdf)                    |

